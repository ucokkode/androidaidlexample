// IColorInterface.aidl
package com.example.aidlservice;

// Declare any non-default types here with import statements

interface IColorInterface {
    int getColor();
}