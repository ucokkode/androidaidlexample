package com.example.aidlservice

import android.app.Service
import android.content.Intent
import android.graphics.Color
import android.os.IBinder
import android.util.Log
import java.util.Random

const val TAG = "ColorServiceLog"

class ColorService : Service() {
    private val mBinder = object : IColorInterface.Stub() {
            override fun getColor(): Int {
                Log.d(TAG, "getColor: Fired!! on ${Thread.currentThread().name}")
                val rnd = Random()
                return Color.argb(255, rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255))
            }

        }

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind: fired!")
        return mBinder
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy: fired!")
    }
}